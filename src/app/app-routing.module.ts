import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
     path:'',
     component:HomepageComponent
  },
  {
     path:'login',
     component:LoginComponent
  },
 {
   path:'signup',
   component:SignupComponent,
 },
{
  path:'home',
  component:HomepageComponent
},
{
   path:'admin',
   loadChildren:()=>AdminModule,
   canActivate:[AuthGuard]
},
{
    path:'user',
    loadChildren:()=>UserModule,
    canActivate:[AuthGuard]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

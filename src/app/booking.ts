import { User } from './user';
import { Room } from './room';

export class Booking {
    bookingId:number
    bookingDate: String
    user: User
    room: Room

}

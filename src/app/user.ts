export class User {
    userId: number
    userName: string
    password: string
    gender: string
    address: string
    userRole: string
}

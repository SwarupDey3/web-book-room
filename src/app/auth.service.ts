import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  logIn:boolean=false
  
  constructor(private myRoute:Router) { }
  sendToken(token: string) {

    localStorage.setItem("LoggedInUser", token)
    //this.logIn=true;

  }
  
  
  // sendLogIn(){
  //   return this.logIn;
  // }

  getToken() {
    return localStorage.getItem("LoggedInUser");
  }
 
  isLoggedIn() {
    return this.getToken() != null
  }
  
  logout() {
    localStorage.removeItem("LoggedInUser");
    this.myRoute.navigate(["/Login"]);
  }
}

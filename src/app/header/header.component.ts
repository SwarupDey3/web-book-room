import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private route:Router,private auth:AuthService) { }

  logIn:boolean=false;
  ngOnInit() {
  }

  login(){
    this.route.navigate(['/login']);
    this.logIn=true;
  }

  logOut(){
    this.auth.logout();
    this.logIn=false
    this.route.navigate(['/home'])
  }
}

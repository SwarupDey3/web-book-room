import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../shared.service';
import { FormBuilder } from '@angular/forms';
import { error } from '@angular/compiler/src/util';
import { User } from '../user';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  ngOnInit(): void {
   this.getUser;
  }

  constructor(private serve:SharedService,private fb:FormBuilder,private route:Router,private authe:AuthService) { }

  flag:boolean=false

  user: User
  loginForm=this.fb.group({

    userName:[''],
    password:['']
  })
  
  getUser(){
   this.user = this.serve.getLogin(this.loginForm.value.userName);
    console.log(this.user);
     if(this.user.password==this.loginForm.value.password)
       return this.flag=true;
      
    
  }

  shareUser(){
    return this.serve.shareUser(this.user);
  }

  login(){
     this.authe.sendToken(this.loginForm.value.userName);
     this.route.navigate(['/home'])
  }

  checkLogin(){
    this.getUser();
    this.login();
    if(this.flag==true){
      if(this.user.userRole=="Admin"||this.user.userRole=="admin")
      this.route.navigate(['/admin'])
      else
     this.route.navigate(['/user']);
    }
    else
       {alert("Wrong Password!!!")
        this.route.navigate(['/home']);}
  
}
}

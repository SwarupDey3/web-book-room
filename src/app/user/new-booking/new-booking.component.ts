import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { User } from 'src/app/user';
import { SharedService } from 'src/app/shared.service';
import { Room } from 'src/app/room';
import { ActivatedRoute, Router } from '@angular/router';
import { Booking } from 'src/app/booking';


@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.css']
})
export class NewBookingComponent implements OnInit {

  constructor(private fb:FormBuilder,private serve:SharedService,private route:Router,private active:ActivatedRoute) { }

  booking:Booking
  user:User;
  room:Room;
  roomNo:number
  model:String
 
//  buildForm(){
//   return this.fb.group({
//     date:['']
//   })
// }
  
  
  ngOnInit() {

    
    this.user=this.serve.returnUser();
    console.log(this.user)
    this.roomNo=(parseInt)(this.active.snapshot.paramMap.get("id"));
    this.serve.getOneUser(this.roomNo).subscribe(data=>{

      this.room=data;
      console.log(this.room)
    })

   
  }

  onSubmit(){
  
    console.log(this.model)
    this.booking.bookingDate=this.model;
    this.booking.room=this.room;
    this.booking.user=this.user;
    console.log(this.booking);
    this.serve.saveBooking(this.booking).subscribe(data=>{

    },err=>{
      alert("Booking Confirmed...")
      this.route.navigate(['/rooms'])
    })

  }

  

  
  
}

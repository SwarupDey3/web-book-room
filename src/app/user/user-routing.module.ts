import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPageComponent } from './user-page/user-page.component';
import { NewRoomComponent } from '../admin/new-room/new-room.component';
import { NewBookingComponent } from './new-booking/new-booking.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  {
    path:'',
    component:UserPageComponent
  },
  {
    path:'booking',
    component:NewBookingComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'booking/:id',
    component:NewBookingComponent,
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }

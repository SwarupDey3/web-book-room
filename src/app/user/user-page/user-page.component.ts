import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { Room } from 'src/app/room';
import { checkAndUpdateBinding } from '@angular/core/src/view/util';
import { NgModel } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  constructor(private serve:SharedService,private route:Router,private active:ActivatedRoute) { }

  ngOnInit() {
   
  }

  
 // type:String;
  rooms:Room
  onSelect(type:string){
    console.log(type)
    this.serve.getRooms(type).subscribe(data=>{
      this.rooms=data;
    })
  }

  onBook(id:number){
    this.route.navigate(['/booking',id]);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPageComponent } from './user-page.component';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/shared.service';
import { type } from 'os';

describe('UserPageComponent', () => {
  let component: UserPageComponent;
  let fixture: ComponentFixture<UserPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[Router,ActivatedRoute],
      declarations: [ UserPageComponent ],
      providers:[ SharedService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('on select working',()=>{
    component.onSelect("Luxury");
    expect(component.onSelect).toBeTruthy();
  })
});

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user';
import { Room } from './room';
import { Booking } from './booking';

@Injectable({
  providedIn: 'root'
})
export class SharedService {


  user:User;
  constructor(private http:HttpClient) { }

  url:String ="http://13.59.213.242:8080/booking";

  //user

  getUser(userName:String): Observable<any>{
  return this.http.get<any>(`${this.url}/user/getuser/${userName}`);
  }

  saveUser(user:User): Observable<any>{
    console.log(user)
    return this.http.post<any>(`${this.url}/user/saveuser`,user);
  }

  getRooms(room:String): Observable<any>{
    return this.http.get<any>(`${this.url}/user/getrooms/${room}`)
  }

  getLogin(userName:String){

    this.getUser(userName).subscribe(data=>{
      this.user=data;
    })

    return this.user;
  }

  saveBooking(booking:Booking): Observable<any>{
    return this.http.post<any>(`${this.url}/user/savebooking`,booking);
  }

  getOneUser(id:number): Observable<any>{
    return this.http.get<any>(`${this.url}/user/getoneroom/${id}`);
  }

  shareUser(user1:User){
    this.user=user1;
    
  }
  
  returnUser(){
    this.shareUser
    return this.user;
  }
 
 
  //admin

  saveRoom(room:Room): Observable<any>{

    return this.http.post<any>(`${this.url}/admin/saveroom`,room);

  }

  getAllRooms(): Observable<any>{
    return this.http.get<any[]>(`${this.url}/admin/getallrooms`);
  }

  deleteRoom(roomNo:any): Observable<any>
  {
    const url2=`${this.url}/admin/deleteroom/${roomNo}`;
    
    console.log(url2)
    return this.http.get(url2);
  }
}
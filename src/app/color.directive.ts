import { Directive,ElementRef,Input,OnInit } from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective implements OnInit {
  

  

  @Input() appColor: string;


  constructor(private ele:ElementRef) { }
  
  ngOnInit(): void {
  
    this.ele.nativeElement.style.backgroundColor=this.appColor;

  }
}

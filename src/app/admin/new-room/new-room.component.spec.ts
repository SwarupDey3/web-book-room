import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRoomComponent } from './new-room.component';
import { FormBuilder } from '@angular/forms';

describe('NewRoomComponent', () => {
  let component: NewRoomComponent;
  let fixture: ComponentFixture<NewRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormBuilder],
      declarations: [ NewRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('save room is working',() =>{
    expect(component.test).toEqual("test is done!");
  })
});

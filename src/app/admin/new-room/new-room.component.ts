import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';
import { Room } from 'src/app/room';

@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.css']
})
export class NewRoomComponent implements OnInit {

  constructor(private fb:FormBuilder,private serve:SharedService) { }

  roomForm=this.fb.group({
    roomNo:[''],
    roomType:[''],
    roomPrice:['']
  })

  test(){
    return "test is done!";
  }
  ngOnInit() {
  }
  room:Room = this.roomForm.value;
  saveRoom(){
   
    this.serve.saveRoom(this.roomForm.value).subscribe((data)=>{
      console.log(data)
      this.roomForm.reset;
     
  },(err)=>{
    console.log();
    alert("Added");
    this.roomForm.reset();
    })

    
    }
  
}

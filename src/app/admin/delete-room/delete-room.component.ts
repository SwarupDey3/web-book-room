import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';
import { Room } from 'src/app/room';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-room',
  templateUrl: './delete-room.component.html',
  styleUrls: ['./delete-room.component.css']
})
export class DeleteRoomComponent implements OnInit {

  constructor(private serve: SharedService,private route:Router) { }

  roomList:Room[]=[]
  ngOnInit() {
    this.serve.getAllRooms().subscribe(data=>{

      this.roomList=data
    })
  }

  delete(roomNo){

    console.log(roomNo)
    this.serve.deleteRoom(roomNo).subscribe(s=>{
      console.log(s);
      
    },err=>{
      alert("deleted")
       location.reload();
    });

    
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { NewRoomComponent } from './new-room/new-room.component';
import { DeleteRoomComponent } from './delete-room/delete-room.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  {
    path:'',
    component:AdminPageComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'new',
    component:NewRoomComponent,
    canActivate:[AuthGuard]
  },
  {
    path:'delete',
    component:DeleteRoomComponent,
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

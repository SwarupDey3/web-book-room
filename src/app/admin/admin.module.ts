import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { NewRoomComponent } from './new-room/new-room.component';
import { DeleteRoomComponent } from './delete-room/delete-room.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AdminPageComponent, NewRoomComponent, DeleteRoomComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }

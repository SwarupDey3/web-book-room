import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { User } from '../user';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private route:Router,private fb:FormBuilder,private serve:SharedService) { }

  user: User=new User();
  signupForm=this.fb.group({
    userName:[''],
    password:[''],
    gender:[''],
    address:['']
  })
  submit(){
    console.log(this.signupForm.value.userName)
    this.user.userName=this.signupForm.value.userName;
    this.user.password=this.signupForm.value.password;
    this.user.gender=this.signupForm.value.gender;
    this.user.address=this.signupForm.value.address;
    this.user.userRole="User";
    this.serve.saveUser(this.user).subscribe(data=>{
      alert(data);
    },err=>{
      alert("Submitted")
      this.route.navigate(['/login'])
    })
  }
  ngOnInit() {

  }

  // login(){
  //   this.route.navigate(['/login']);
  // }
}
